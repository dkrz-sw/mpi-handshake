#!/usr/bin/bash

mpirun -n 1 ./model.x A yac: \
       -n 1 ./model.x B yac foo : \
       -n 1 ./model_fortran.x yac fortran foo :\
       -n 2 ./model_fortran.x foo

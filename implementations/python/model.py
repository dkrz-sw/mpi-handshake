#!/usr/bin/env python3
from mpi_handshake import mpi_handshake
import sys

comms = mpi_handshake(sys.argv[1:])

print("My comms are:")
for name, comm in comms.items():
    print(f"{name}: I'm rank {comm.rank} of {comm.size}")

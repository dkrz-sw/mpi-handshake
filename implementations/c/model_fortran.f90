PROGRAM model

  USE mpi
  USE mo_mpi_handshake, ONLY: mpi_handshake, MAX_GROUPNAME_LEN

  CHARACTER(len=MAX_GROUPNAME_LEN), ALLOCATABLE :: groups(:)
  INTEGER :: i, ierr, comm_rank, comm_size
  INTEGER, ALLOCATABLE :: comms(:)

  ALLOCATE(groups(command_argument_COUNT()))
  ALLOCATE(comms(command_argument_COUNT()))

  DO i=1,COMMAND_ARGUMENT_COUNT()
    CALL GET_COMMAND_ARGUMENT(i, groups(i))
  END DO

  CALL MPI_Init(ierr)
  CALL mpi_handshake(MPI_COMM_WORLD, groups, comms)

  DO i=1,COMMAND_ARGUMENT_COUNT()
    CALL MPI_Comm_rank(comms(i), comm_rank, ierr)
    CALL MPI_Comm_size(comms(i), comm_size, ierr)
    WRITE(0,*) groups(i), comm_rank, comm_size
  END DO
  CALL MPI_Finalize(ierr)

END PROGRAM model

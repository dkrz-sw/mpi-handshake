#include <stdlib.h>
#include <mpi.h>

#include "mpi_handshake.h"

int main(void) {

  MPI_Init(NULL, NULL);

  mpi_handshake_dummy(MPI_COMM_WORLD);

  MPI_Finalize();

  return EXIT_SUCCESS;
}

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mpi_handshake.h"

int main(int argc, char** argv) {

  MPI_Init(&argc, &argv);

  MPI_Comm * group_comms = malloc((argc-1)*sizeof(group_comms));

  mpi_handshake((const char**)argv+1, group_comms, argc-1, MPI_COMM_WORLD);

  for(int i=0; i<argc-1; ++i){
    int rank, size;
    MPI_Comm_rank(group_comms[i], &rank);
    MPI_Comm_size(group_comms[i], &size);
    printf("In %s im rank %d of %d\n", argv[i+1], rank, size);
  }

  MPI_Finalize();

  return EXIT_SUCCESS;
}

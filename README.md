# MPI Handshake

## Purpose
- Define a as-simple-as-possible way of splitting `MPI_COMM_WORLD` up into different communicators.
- Every process can attend at multiple communicators
- Communicators are identfied by names (strings), also called groups
- The algorithm is MPI-2.1 compatible
- A hierachical splitting is possible by calling the algorithm multiple times

### Example

1. ICON, FESOM, external ComIn process call handshake routine with input argument `MPI_COMM_WORLD`.
    - ComIn and ICON belong to group ComIn
    - FESOM and ICON belong to YAC
    - ICON belongs to group ICON
    - FESOM belongs to group FESOM
2. FESOM and ICON initializes YAC with the YAC group communicator
3. ICON internally splits its communicator into components (eg. atm, oce)
4. ICON and the external ComIn call the handshake algorithm once more:
    - ICON-A and the external ComIn belong to group ComIn+ICON
    - ICON-O does not take part in this group
5. (Optionally:) YAC could be involved a second time, this time YAC is initialized with the communicator ComIn+ICON

## Algorithm
### Pseudocode
- Step 1: Exchange version information
  ```
  MPI_Allreduce(1, min_version, 1, MPI_INT, MPI_MIN, comm)
  assert(min_version == 1)
  ```
- Step 2: Determine broadcasting rank
    - The rank with the smalles rank number that still needs to create a
      communicator for at least one of its groups is the next
      broadcasting rank.
    - This is determined by a
  ```
  MPI_Allreduce(..., 1, MPI_INT, MPI_MIN, comm)
  ```
    - If the result of this allreduce is equal to the communicator size,
      all necessary communicators are constructed and the algorithm
      terminates.
- Step 3: Broadcast the groupname
    - The groupname for the next commuinicator split is broadcasted by
      the rank that was determined in the last step.
    - This is done by broadcasting the string length first followed by
      the broadcast of the actual string
  ```
  MPI_Bcast(broadcasting_groupname_len, 1, MPI_INT, broadcaster, comm)
  MPI_Bcast(broadcasting_groupname, broadcasting_groupname_len, MPI_CHAR, broadcaster, comm)
  ```
    - Remark: To be implemeentation independed, the string is
      broadcasted without a terminating null-character.
- Step 4: Splitting the communicator
    - Now every process decides depending on its groupname list whether
      it wants to be part of the communicator not.
    - The split is then done by calling
  ```
  MPI_Comm_split(comm, color=0, new_comm)
  ```
    - If the process dont want to be part of the communicator it should
      pass `color=MPI_UNDEFINED`.
    - Goto Step 2

### Activity diagram
![Activity diagram for the MPI Handshake algorithm](https://www.plantuml.com/plantuml/svg/bP9VIyCm5CNVyockUsgX0_UgJFPd4_km8g9F8YMsTsimJKgIEbJstKtwdp87xYdBFVdTSsxlGXkgJ9bpScKop14AJfaWrIsvsJs6yP9pXMcPe3SBu1ELPb84OC-k64RFzM4NHW4aCi_zMyBsu3N2-NpcWmKBy8JqBLOM14NAzWJWc36EuFcaiBsOiMgORD2Wofb0U5UIfWdLXea3A2e-9l1301oRSWu79Sj2q1mrKAtPGM3QDlf2xHEmlv5zpRunwBceBgS0GAxH-JvBSvR-HQjZdveCWuUjR-UlRkN6rFJ_G-kwDxOqSamCbAATGp-RhkrmnAkMrklSQ9k0ArVnkahCSVInbPHcFepT9JmB2634Sfhrm_Bf0kaqjAiBpanLAGLBg96gNuGE3Km34y1bGklLi8-qERyGQukAAwhdg05S1o3m67SnhAuUPNSDHlOnXfjn2UikNwBDzZwCjflH1jXJjVtsE7tDM9gYU9lUsNyf6ffaP645PSv_0G00)

## Outcome
- `MPI_Comm` for each group that was passed in
- Other information that can be derived from the algorithm:
    - globally consistent order of communicators (could be relevant to avoid
      deadlocks e.g. if hierachies are created afterwards)
    - Rank leader for each group (also for remote groups) - could be
      used for Inter-Communicator creation

## Language interoperability

In principle it is also possible to implement this algorithm in Fortran.
However, interoperability between Fortran and C MPI datatypes is not portable
(`MPI_INTEGER` is not necessary the same as `MPI_INT`).
A straight-forward implementation would rely on the compatibility of datatypes 
in a particular MPI implementation.
(see https://www.mpi-forum.org/docs/mpi-2.2/mpi22-report.pdf Section 16.3.10)

This makes the implementation of Fortran version of this algorithm, which
is compatible with a C version, difficult. Therefore, it is advised to
use a C implementation and provide a Fortran interface for this
implementation.

An example can be found in the c implmentation.

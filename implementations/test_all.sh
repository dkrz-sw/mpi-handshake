#!/usr/bin/bash

mpirun -n 1 python/model.py python yac : \
       -n 1 c/model.x A yac : \
       -n 2 c/model.x B yac : \
       -n 3 python/model.py python foo bar : \
       -n 1 c/model.x C foo : \
       -n 1 python/model.py : \
       -n 1 c/model_dummy.x : \
       -n 4 python/model.py yac
